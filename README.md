# Tutorial: Extending autoconvert

This is the source code for the autoconvert-dev-docs tutorial: [Extending Autoconvert][1].

It changes the link text on developer.atlassian.com pages to the Page name.

## Running locally

To run this app locally, make sure that you have the [Atlassian Plugin SDK][2] installed, and then run:

    atlas-mvn confluence:run
    
 [1]: https://developer.atlassian.com/display/CONFDEV/Extending+Autoconvert
 [2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project